<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Kochbuch extends Component
{
    public function render()
    {
        return view('livewire.kochbuch');
    }
}
