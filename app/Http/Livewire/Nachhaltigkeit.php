<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Nachhaltigkeit extends Component
{
    public function render()
    {
        return view('livewire.nachhaltigkeit');
    }
}
