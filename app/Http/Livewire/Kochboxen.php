<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Kochboxen extends Component
{
    public function render()
    {
        return view('livewire.kochboxen');
    }
}
