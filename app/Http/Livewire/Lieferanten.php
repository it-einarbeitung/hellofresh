<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Lieferanten extends Component
{
    public function render()
    {
        return view('livewire.lieferanten');
    }
}
