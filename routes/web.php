<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/',\App\Http\Livewire\Welcome::class)->name('welcome');

Route::get('/kochboxen',\App\Http\Livewire\Kochboxen::class)->name('kochboxen');

Route::get('/funktioniert',\App\Http\Livewire\Funktioniert::class)->name('funktioniert');

Route::get('/nachhaltigkeit',\App\Http\Livewire\Nachhaltigkeit::class)->name('nachhaltigkeit');

Route::get('/klimaneutralität',\App\Http\Livewire\Klimaneutralität::class)->name('klimaneutralität');

Route::get('/lieferanten',\App\Http\Livewire\Lieferanten::class)->name('lieferanten');


Route::get('/menu',\App\Http\Livewire\Menu::class)->name('menu');

Route::get('/kochbuch',\App\Http\Livewire\Kochbuch::class)->name('kochbuch');




