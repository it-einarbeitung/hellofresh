<div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-dark h1">
                    <b>So funktioniert .......</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/hiw-plan-2.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-center mt-5">
                <p class="text-dark h3 mt-5">
                    Wie verbessert Criteo Ihre Werbeerfahrung?
                </p>
                <p class="text-dark h5 mt-3">
                    Das Ziel von Criteo ist es, Ihnen die Produkte und Dienstleistungen zu zeigen, an denen Sie am wahrscheinlichsten interessiert sind.
                </p>
                <button class="btn btn-outline-success bg-success text-light mt-3 " style="width:230px; height: 50px;">
                    <b>Senden anehen</b>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 text-center mt-5">
                <p class="text-dark h3 mt-5">
                    Wie verbessert Criteo Ihre Werbeerfahrung?
                </p>
                <p class="text-dark h5 mt-3">
                    Das Ziel von Criteo ist es, Ihnen die Produkte und Dienstleistungen zu zeigen, an denen Sie am wahrscheinlichsten interessiert sind.
                </p>
                <button class="btn btn-outline-success bg-success text-light mt-3 " style="width:230px; height: 50px;">
                    <b>Senden anehen</b>
                </button>
            </div>
            <div class="col-lg-6 text-center">
                <img src="/images/hiw-delivery_de.jpg" class="img-fluid">
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-6 text-center">
                <img src="/images/hiw-family-4.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-center mt-5">
                <p class="text-dark h3 mt-5">
                    Wie verbessert Criteo Ihre Werbeerfahrung?
                </p>
                <p class="text-dark h5 mt-3">
                    Das Ziel von Criteo ist es, Ihnen die Produkte und Dienstleistungen zu zeigen, an denen Sie am wahrscheinlichsten interessiert sind.
                </p>
                <button class="btn btn-outline-success bg-success text-light mt-3 " style="width:230px; height: 50px;">
                    <b>Senden anehen</b>
                </button>
            </div>
        </div>
    </div>
    <div class="bg-light py-5">
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <p class="text-dark h1">
                    <b>6 Gründe für ....</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-3 text-start">
                <i class="far fa-smile h1"></i>
                <p class="text-dark h5 mt-3">
                    <b>Lecker</b>
                </p>
                <p class="text-dark h5 mt-3">
                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
            <div class="col-lg-3 text-start">
                <i class="fal fa-handshake h1"></i>
                <p class="text-dark h5 mt-3">
                    <b>Einfach</b>
                </p>
                <p class="text-dark h5 mt-3">
                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
            <div class="col-lg-3 text-start">
                <i class="fab fa-buromobelexperte h1"></i>
                <p class="text-dark h5 mt-3">
                    <b>Flexibel</b>
                </p>
                <p class="text-dark h5 mt-3">
                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-3 text-start">
                <i class="fas fa-beer h1"></i>
                <p class="text-dark h5 mt-3">
                    <b>Stressfrei</b>
                </p>
                <p class="text-dark h5 mt-3">
                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
            <div class="col-lg-3 text-start">
                <i class="fas fa-burger-soda h1"></i>
                <p class="text-dark h5 mt-3">
                    <b>Frisch</b>
                </p>
                <p class="text-dark h5 mt-3">
                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
            <div class="col-lg-3 text-start">
                <i class="far fa-circle h1"></i>
                <p class="text-dark h5 mt-3">
                    <b>Nachhaltig</b>
                </p>
                <p class="text-dark h5 mt-3">
                     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <button class="btn btn-outline-success bg-success text-light mt-3 " style="width:230px; height: 50px;">
                    <b>Senden anehen</b>
                </button>
            </div>
        </div>
    </div>
</div>
