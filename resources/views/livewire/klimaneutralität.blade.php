<div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <img src="/images/websiteplanet-dummy-1450X240.png" class="img-fluid">
        </div>
    </div>
    <div class="d-none d-lg-block">
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-6 col-md-12">
                <p class="text-success h3" style="margin-top:-140px;">
                    <b>Die Standardpassage <br>des Lorem</b>
                </p>
                <p class="text-light h5">
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                </p>
                <p class="text-light h5">
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/hiw-delivery_de.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/carbon_offset_2021_Kenya.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltige Land- und Forstwirtschaft in Kenia</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Der Westen Kenias ist eine der ärmsten Regionen des Landes, gut 22 Millionen Menschen leben in absoluter Armut. Die meisten Menschen im ländlichen Kenia bewirtschaften Parzellen von weniger als einem Hektar. Durch die extreme Dürre, unfruchtbare Böden und falsche Anbaumethoden bleibt die Ernte aus und Familien können sich nicht mehr ernähren. Der Hunger schwächt die Menschen und besonders Kinder und ältere Menschen leiden sehr.
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/mandu6.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltige Land- und Forstwirtschaft in Kenia</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Der Westen Kenias ist eine der ärmsten Regionen des Landes, gut 22 Millionen Menschen leben in absoluter Armut. Die meisten Menschen im ländlichen Kenia bewirtschaften Parzellen von weniger als einem Hektar. Durch die extreme Dürre, unfruchtbare Böden und falsche Anbaumethoden bleibt die Ernte aus und Familien können sich nicht mehr ernähren. Der Hunger schwächt die Menschen und besonders Kinder und ältere Menschen leiden sehr.
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/ow.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Erneuerbare Energie</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Als Teil unseres Ziels, bis zum Jahr 2040 CO2-neutral zu sein, ist Amazon auf dem Weg, seine Betriebsabläufe bis 2030 zu 100 % mit erneuerbaren Energien zu versorgen — und wir sehen uns auf einem guten Weg, dieses Ziel sogar fünf Jahre früher zu erreichen. Im Jahr 2020 wurden wir zum weltgrößten Abnehmer von erneuerbaren Energien und erreichten einen Anteil von 65 % an erneuerbaren Energien im gesamten Unternehmen.
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/olia1_Low.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Über Planetly</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Planetly ist ein Climate-Tech-Unternehmen, das digitale Tools entwickelt, mit denen Unternehmen ihren CO2-Fußabdruck auf der Grundlage internationaler Standards berechnen, reduzieren und kompensieren können.
                    Die innovative Softwarelösung von Planetly versetzt Unternehmen in die Lage, ihre Emissionen ganzheitlich zu verwalten. Dabei wird der CO2-Fußabdruck transparent analysiert, durch effektive Maßnahmen reduziert und mit Hilfe von zertifizierten Klimaprojekten ausgeglichen.
                </p>
                
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
</div>
