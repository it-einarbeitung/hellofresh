<div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <img src="/images/websiteplanet-dummy-1450X240.png" class="img-fluid">
        </div>
    </div>
    <div class="d-none d-lg-block">
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-6 col-md-12">
                <p class="text-success h3" style="margin-top:-140px;">
                    <b>Die Standardpassage <br>des Lorem</b>
                </p>
                <p class="text-light h5">
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                </p>
                <p class="text-light h5">
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <p class="text-dark h1 mt-5">
                <b>Unsere eiusmod : von Natur aus nachhaltig</b>
            </p>
            <p class="text-dark h5 mt-3">
                 Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
            </p>
            <p class="text-dark h5">
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <p class="text-dark h5">
                 Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            </p>
        </div>  
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/i_v2_DE.jpg" class="img-fluid">
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/i_v2_DE.jpg" class="img-fluid">
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/_report.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/_one_third.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/o2.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/3_Packaging_rec_6.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/ing.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center">
            <p class="text-dark h3">
                <b>Warum nutzen wir es?</b>
            </p>
            <p class="text-dark h5 mt-3">
                Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird, wenn er sich ein Layout ansieht.
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 text-center">
            <img src="/images/unity.jpg" class="img-fluid">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center">
            <p class="text-dark h1 mt-5">
                <b>Was ich tun kann</b>
            </p>
            <p class="text-dark h5 mt-3">
                Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr,
            </p>
            <p class="text-dark h5">
                 was ihn über 2000 Jahre alt macht. Richar McClintock, 
            </p>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/el_Tip1_Potatoes.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Kartoffelpüree</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Kartoffelpüree oder Erdäpfelpüree
                </p>
                <p class="text-dark h5">
                     passt zu zahlreichen Gerichten
                </p>
                <p class="text-dark h5">
                    und kann je nach Belieben
                </p>
                <p class="text-dark h5">
                    Für ein Kartoffelpüree
                </p>
                <p class="text-dark h5">
                    sollten mehlige Kartoffeln
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/E_Carousel_Tip3_Rice.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Reis</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Zwiebel schälen,
                </p>
                <p class="text-dark h5">
                    in feine Stücke schneiden.
                </p>
                <p class="text-dark h5">
                    In einem großen Topf
                </p>
                <p class="text-dark h5">
                    die Butter zerlassen 
                </p>
                <p class="text-dark h5">
                    und erhitzen
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/sel_Tip3_Chicken.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Hähnchenfleisch</b>
                </p>
                <p class="text-dark h5 mt-3">
                   Zuerst Erbsenschoten auftauen
                </p>
                <p class="text-dark h5">
                    waschen und beiseite stellen.
                </p>
                <p class="text-dark h5">
                    Das Fleisch klein schneiden
                </p>
                <p class="text-dark h5">
                    und im Wok mit etwas 
                </p>
                <p class="text-dark h5">
                    und erhitzen Sesamöl  
                </p>
                <p class="text-dark h5">
                    einige Minuten gut 
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/ousel_Tip4_Ham.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Schinken</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Für die Schinkenfleckerl
                </p>
                <p class="text-dark h5">
                     in einem großen Topf
                </p>
                <p class="text-dark h5">
                     Salzwasser zum Kochen
                </p>
                <p class="text-dark h5">
                    bringen, die Fleckerl  
                </p>
                <p class="text-dark h5">
                    zufügen
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
</div>
