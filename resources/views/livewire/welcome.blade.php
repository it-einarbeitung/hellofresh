<div>
    <div class="row">
        <div class="col-lg-12 col-lg-3 text-center">
            <img src="/images/tografie-00750-p-1600.jpg" class="img-fluid">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center"style="margin-top: -250px;">
            <p class="text-success  h1">
                <b>Wir sparen Dir Zeit</b>

            </p>
            <button class="btn btn-outline-success bg-success text-light">
                   <b> Kochboxen ansehen</b>
            </button>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-12 text-center">
           <p class="text-dark h2">
                <b>So funktioniert's</b>
           </p>
           <p class="text-secondary h5">
                General Data We . 
                Use of Your Personal information  .
                Disclosure of Personal information
           </p>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/hiw-1-v2.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3">
                    Pizza mit Salat
                </p>
                <p class="text-dark h5">
                    250 ml handwarmem Wasser auflösen
                    Mehl, Salz zufügen
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/hiw-2-v2.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3">
                    Pizza mit Salat
                </p>
                <p class="text-dark h5">
                    250 ml handwarmem Wasser auflösen
                    Mehl, Salz zufügen
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/hiw-3-v2.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3">
                    Pizza mit Salat
                </p>
                <p class="text-dark h5">
                    250 ml handwarmem Wasser auflösen
                    Mehl, Salz zufügen
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/hiw-4-v3.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3">
                    Pizza mit Salat
                </p>
                <p class="text-dark h5">
                    250 ml handwarmem Wasser auflösen
                    Mehl, Salz zufügen
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <button class="btn btn-outline-success  text-dark" style="width: 170px;">
                    <b>Mehr erfahrn</b>
                </button>
                <p class="text-dark mt-3">
                    Du kannst jederzeit kündigen
                </p>
            </div>
        </div>
    </div>
    <div class="bg-light py-5">
        <div class="container">
            <div class="d-none d-lg-block">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p class="text-dark h1">
                            <b>Gut für Dich, </b>
                        </p>
                        <p class="text-dark h1">
                            <b>Dieses Buch</b>
                        </p>
                        <p class="text-dark h1">
                            <b>und Deinen Geldbeutel</b>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 text-center">
                        <img src="/images/desktop_goodforyou-v2.jpg" class="img-fluid">
                    </div>
                    <div class="col-lg-1 text-end mt-4">
                        <i class="fas fa-hourglass-half text-dark h3"></i>
                        <p class="mt-5">
                           <i class="far fa-usd-circle text-dark h3 mt-5"></i> 
                        </p>
                        <p class="mt-5">
                            <i class="fal fa-planet-moon text-dark h3 mt-5"></i>
                        </p>
                    </div>
                    <div class="col-lg-5 text-start mt-4">
                        <p class="text-dark h5">
                            Zeitersparnis
                        </p>
                        <p class="text-dark h5 mt-3">
                            Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein 
                        </p>
                        <p class="text-dark h5">
                            zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur   
                        </p>
                        <p class="text-dark h5">
                            von 45 v. Chr  
                        </p>
                        <p class="text-dark h5 mt-3">
                            Preis-Leistungs
                        </p>
                        <p class="text-dark h5 mt-3">
                            was ihn über 2000 Jahre alt macht.
                        </p>
                        <p class="text-dark h5">
                            Richar McClintock, ein Lateinprofessor
                        </p>
                        <p class="text-dark h5 mt-5">
                            Reduce your ecological footprint
                        </p>
                        <p class="text-dark h5 mt-3">
                            Der Standardteil von Lorem Ipsum, genutzt seit 1500, ist reproduziert für die, die es interessiert. Sektion 1.10.32 und 1.10.33 von "de Finibus Bonorum et Malroum" von Cicero sind auch reproduziert in ihrer Originalform, abgeleitet von der Englischen Version aus von 1914 (H. Rackham)
                        </p>
                        <p class=" text-center h1">
                        <button class="btn btn-outline-success bg-success text-light mt-5">
                            <b>Starte jetzt</b>
                        </button>
                    </p>
                    </div>
                </div>
            </div>
            <div class="d-block d-lg-none">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p class="text-dark h1">
                            <b>Gut für Dich, </b>
                        </p>
                        <p class="text-dark h1">
                            <b>Dieses Buch</b>
                        </p>
                        <p class="text-dark h1">
                            <b>und Deinen Geldbeutel</b>
                        </p>
                        <img src="/images/desktop_goodforyou-v2.jpg" class="img-fluid">
                        <p class="text-dark h5 mt-3">
                            Zeitersparnis
                        </p>
                        <p class="text-dark h5 mt-3">
                            Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein 
                        </p>
                        <p class="text-dark h5">
                            zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur   
                        </p>
                        <p class="text-dark h5">
                            von 45 v. Chr  
                        </p>
                        <p class="text-dark h5 mt-5">
                            Preis-Leistungs
                        </p>
                        <p class="text-dark h5 mt-3">
                            was ihn über 2000 Jahre alt macht.
                        </p>
                        <p class="text-dark h5">
                            Richar McClintock, ein Lateinprofessor
                        </p>
                        <p class="text-dark h5 mt-5">
                            Reduce your ecological footprint
                        </p>
                        <p class="text-dark h5 mt-3">
                            Der Standardteil von Lorem Ipsum, genutzt seit 1500, ist reproduziert für die, die es interessiert. Sektion 1.10.32 und 1.10.33 von "de Finibus Bonorum et Malroum" von Cicero sind auch reproduziert in ihrer Originalform, abgeleitet von der Englischen Version aus von 1914 (H. Rackham)
                        </p>

                        <button class="btn btn-outline-success bg-success text-light mt-5">
                            <b>Starte jetzt</b>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-12 text-center mt-5">
                    <p class="text-dark h1">
                        <b>Deine Box</b>
                    </p>
                    <p class="text-secondary h5">
                        Der Standardteil von Lorem Ipsum, genutzt seit 1500, ist reproduziert
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 text-center mt-5">
                    <div class="card bg-dark text-white">
                        <img src="/images/el_Tip1_Potatoes.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay">
                            <p class="card-title h2">
                                Für stressige Abende
                            </p>
                            <p class="card-text">
                                Fertig in 15 Minuten
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-center mt-5">
                    <div class="card bg-dark text-white">
                        <img src="/images/E_Carousel_Tip3_Rice.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay">
                            <p class="card-title h2">
                                Für Besuch
                            </p>
                            <p class="card-text">
                                Flexibel anpassbare 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-center mt-5">
                    <div class="card bg-dark text-white">
                        <img src="/images/sel_Tip3_Chicken.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay">
                            <p class="card-title h2">
                                Für (noch) mehr Genuss
                            </p>
                            <p class="card-text">
                                Leckere Beilagen  
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3  text-center mt-5">
                    <div class="card bg-dark text-white">
                        <img src="/images/ousel_Tip4_Ham.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay">
                            <p class="card-title h2">
                                Für besondere Anlässe
                            </p>
                            <p class="card-text">
                                Leckere Beilagen  
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <button class="btn btn-outline-success bg-success text-light mt-5">
                        Menü ansehen
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-dark h1 mt-5">
                    <b>Unsere Kunden essen besser – jeden Tag</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-center mt-5">
                <p class="text-dark h3">
                    <b>Silke sagt</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Der Standardteil von Lorem Ipsum, genutzt seit 1500, ist reproduziert für die, die es interessiert. Sektion 1.10.32 und 1.10.33 von "de Finibus Bonorum et Malroum" von Cicero sind auch reproduziert in ihrer Originalform, 
                </p>
                <p class="mt-4">
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                </p>
            </div>
            <div class="col-lg-6 text-center mt-5">
                <p class="text-dark h3">
                    <b>Clara sagt</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Wörterbuch aus über 200 Lateinischen Wörter, kombiniert mit einer Handvoll Kunstsätzen, welche das Lorem Ipsum glaubwürdig macht. Das generierte Lorem Ipsum ist außerdem frei 
                </p>
                <p class="mt-4">
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                    <i class="fas fa-star text-warning h3"></i>
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <img src="/images/ring-08507-p-1080.jpg" class="img-fluid">
        </div>
    </div>
</div>
