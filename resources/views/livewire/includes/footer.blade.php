<div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-2 mt-5">
                <p class="text-dark ">
                    <b>HELLOFRESH</b>
                </p>
                <a href="#" class="text-dark">
                    Geschenkgutschein
                </a>
                <a href="#" class="text-dark">
                    Studentenrabatt<br>
                </a>
                <a href="#" class="text-dark ">
                    Rezepte<br>
                </a>
                <a href="#" class="text-dark">
                    Blog<br>
                </a>
                <a href="#" class="text-dark">
                    Cookie-Einstellungen
                </a>
                <p class="text-dark  mt-3">
                    <b>ZAHLUNGSARTEN</b>
                </p>
                <img src="/images/mastercard.jpg" class="img-fluid">
                <img src="/images/visa.jpg" class="img-fluid">
                <img src="/images/discovernet.jpg" class="img-fluid">
                <img src="/images/americanexpress.jpg" class="img-fluid">
                <img src="/images/paypal.jpg" class="img-fluid">
            </div>
            <div class="col-lg-2 mt-5">
                <p class="text-dark">
                    <b>UNSER UNTERNEHMEN</b>
                </p>
                <a href="#" class="text-dark">
                     Group<br>
                </a>
                <a href="#" class="text-dark">
                     Jobs<br>
                </a>
                <a href="#" class="text-dark">
                    Presse<br>
                </a>
                <a href="#" class="text-dark">
                    Neuer Look
                </a>
            </div>
            <div class="col-lg-2 mt-5">
                <p class="text-dark">
                    <b>ARBEITE MIT UNS</b>
                </p>
                <a href="#" class="text-dark">
                    Blogger/Influencer<br>
                </a>
                <a href="#" class="text-dark">
                     Affiliates<br>
                </a>
                <a href="#" class="text-dark">
                    Marketingkooperationen<br>
                </a>
                <a href="#" class="text-dark">
                    HelloFresh für<br> 
                </a>
                <a href="#" class="text-dark">
                    Unternehmen
                </a>
            </div>
            <div class="col-lg-2 mt-5">
                <p class="text-dark">
                    <b>HILFE</b>
                </p>
                <a href="#" class="text-dark">
                    Hilfe-Center<br>
                </a>
                <a href="#" class="text-dark">
                     Affiliates<br>
                </a>
                <a href="#" class="text-dark">
                    Finde eine Antwort<br>
                </a>
            </div>
            <div class="col-lg-4 mt-5">
                <p class="text-success">
                    <b>INSTALLIERE UNSERE APP</b>
                </p>
                <p class="text-dark">
                    Verwalte Deine Lieferungen von überall, jederzeit.
                </p>
                <img src="/images/appstore-badge-de.jpg" class="img-fluid" style="width:120px;">
                <img src="/images/playstore-badge-de.jpg" class="img-fluid" style="width:120px;">
            </div>
        </div>
    </div>
</div>
