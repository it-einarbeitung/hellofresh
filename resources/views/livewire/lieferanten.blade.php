<div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <img src="/images/websiteplanet-dummy-1450X240.png" class="img-fluid">
            <p class="text-dark h3 mt-5">
                <b>unseren Lieferanten</b>
            </p>
            <p class="text-dark h5 mt-3">
                Wir erhalten regelmäßig Angebote von Lieferanten, eine Partnerschaft einzugehen.
            </p>
            <p class="text-dark h5">
                . Natürlich arbeiten wir gerne mit anderen Unternehmen zusammen und sind immer an 
            </p>
            <p class="text-dark h5">
                langfristigen Beziehungen interessiert
            </p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-6 text-center">
            <img src="/images/een_box.jpg" class="img-fluid">
            <p class="text-dark h3 mt-3">
                <b>Wo kann ich es kriegen?</b>
            </p>
            <p class="text-dark h5 mt-3">
                Es gibt viele Variationen der Passages des Lorem Ipsum aber 
            </p>
            <p class="text-dark h5">
                der Hauptteil erlitt Änderungen in irgendeiner Form, 
            </p>
            <p class="text-dark h5">
                durch Humor oder zufällige Wörter welche nicht einmal 
            </p>
        </div>
        <div class="col-lg-6 text-center">
            <img src="/images/een_Tractor_new.jpg" class="img-fluid">
            <p class="text-dark h3 mt-3">
                <b>Wo kann ich es kriegen?</b>
            </p>
            <p class="text-dark h5 mt-3">
                Es gibt viele Variationen der Passages des Lorem Ipsum aber 
            </p>
            <p class="text-dark h5">
                der Hauptteil erlitt Änderungen in irgendeiner Form, 
            </p>
            <p class="text-dark h5">
                durch Humor oder zufällige Wörter welche nicht einmal 
            </p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
        <p class="text-dark h3 mt-5 text-center">
            <b>Lieferanten</b>
        </p>
        <p class="text-dark h5 mt-3 text-center">
            Wir erhalten regelmäßig Angebote von Lieferanten, eine Partnerschaft einzugehen.
        </p>
        <p class="text-dark h5 text-center">
            . Natürlich arbeiten wir gerne mit anderen Unternehmen zusammen und sind immer an 
        </p>
        <p class="text-dark h5 text-center">
            langfristigen Beziehungen interessiert
        </p>
        <hr class="border border-1 border-dark mt-5">
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/inability.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/lity_Uhrbach.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/ility_.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/pply.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/DSC_0428.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/_800x534_1.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>Nachhaltigkeitsbericht 2021</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney College in Virgnia untersuche einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage und fand eine unwiederlegbare Quelle.
                </p>
                <button class="btn btn-outline-success bg-success text-light text-center mt-4" style="width:200px;">
                    Mehr erfahren
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
</div>
