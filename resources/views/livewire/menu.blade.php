<div class="bg-light py-5">
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/oli-58275515.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                                Family
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/reis-34c6eb4a.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>45 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                                Higen Protein
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/alat-2a780fe8.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                                Vegetarisch • Family • Ohne Weizen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/orzwiebeln-7726ee41.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>40 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                                Family 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/nne-4db05e1f.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                               Ohne Milchprodukte
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/ase-tacos-35c324c1.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/e-6e0a5e8a.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                                Vegetarisch 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/6a983e3.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>30 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                               Vegetarisch
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/use-36e13211.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/en-krautern-378f4af9.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>100 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/con-b8ccab9e.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>50 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                               Ohne Weizen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/emuse-36e13211.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>45 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Klimaheld • Vegetarisch • Family • Ohne Weizen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/fen-244e9d3f.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                               Family • unter 650 Kalorien
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/osze-02738314.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>40 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Family
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/vocado-95c302f8.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>15 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Zeit sparen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/3f7b.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>15 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                               Zeit sparen • Family • Ohne Weizen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/m-salat-34de40ae.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>50 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch • Ohne Weizen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/-taschen-66a983e3.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch • Ohne Weizen
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/urryreis-29e3683e.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                               High Protein 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/cae5de.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>15 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Zeit sparen • Family 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/90.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Family
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/schkase-93ddd314.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>40 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/richt-96c1d967.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Family 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/kener-birne-42f19b69.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Family
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/a1.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegan  
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/a2.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>15 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Family 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/a3.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Ohne Weizen 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/b1.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>15 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch  
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/b2.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>30 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/b3.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>50 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Ohne Weizen 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/c1.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style="border-radius: 0px;">
                    <img src="/images/menu/c2.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>30 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Vegetarisch 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/c3.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>40 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Ohne Weizen 
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-4 text-center mt-5">
                <div class="card" style=" border-radius: 0px;">
                    <img src="/images/menu/c4.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">
                            Some quick example text to build on the card title and make up the bulk of the card's content.
                        </p>
                        <p class="card-text text-start">
                            <small class="text-dark">
                                <b>35 min</b>
                            </small>
                            <span class="border-end ms-2"></span>
                            <small class="text-muted ms-2">
                              Family
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <p class="card-text h3">
                    <b>Anschließend Öl in einem weiteren Topf heiß werden lassen</b>
                </p>
                <button class="btn btn-outline-success bg-success text-light mt-3" style="width:230px;">
                    Essen ansehen
                </button>
            </div>
        </div>
    </div>
</div>
