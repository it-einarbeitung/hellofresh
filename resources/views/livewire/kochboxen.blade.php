<div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-dark mt-5 h1">
                Was für Rezepte magst Du?
            </p>
            <p class="text-secondary h5">
                Wähle bis zu 3 Vorlieben aus. Du kannst sie später immer ändern!
            </p>
            <div class="btn-group mt-5" role="group" aria-label="Basic checkbox toggle button group">
                <input type="checkbox" class="btn-check" id="btncheck1" autocomplete="off">
                    <label class="btn btn-outline-success" for="btncheck1">
                        Fleisch & Co
                    </label>

                <input type="checkbox" class="btn-check" id="btncheck2" autocomplete="off">
                    <label class="btn btn-outline-success" for="btncheck2">
                        Familienfreundlich
                    </label>

                <input type="checkbox" class="btn-check" id="btncheck3" autocomplete="off">
                    <label class="btn btn-outline-success" for="btncheck3">
                        Balance
                    </label>
            </div>
            <p class="text-secondary mt-5">
                Koche Deine Gerichte mit Fleisch, Fisch und einer Auswahl an saisonalen Produkten.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-4 text-end">
              <p class="text-secondary">
                Thermomix-geeignete Rezepte
            </p>
        </div>
        <div class="col-lg-6 text-center">
            <div class="form-check form-switch text-center">
                <input class="form-check-input ms-5" type="checkbox" role="switch" id="flexSwitchCheckDefault">
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-dark h1">
                Wie groß soll Dein Menü sein?
            </p>
            <p class="text-dark mt-3 h5">
                Dies wird als Deine Standardgröße gespeichert, Du kannst sie aber 
            </p>
            <p class="text-dark h5">
                immer wieder anpassen
            </p>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-3"></div>
            <div class="col-lg-3  text-cente mt-5">
                <p class="text-dark h5">
                    Anzahl der Personen
                </p>
            </div>
            <div class="col-lg-3 text-center mt-5">
                <div class="btn-group" role="group" >
                    <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off">
                    <label class="btn btn-outline-success" for="btnradio1" aria-label="Basic radio toggle button group" data-bs-toggle="collapse" href="#btnradio1" aria-expanded="false" aria-controls="btnradio1">
                         2
                    </label>
                    <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
                    <label class="btn btn-outline-success" for="btnradio2" aria-label="Basic radio toggle button group" data-bs-toggle="collapse" href="#btnradio2" aria-expanded="false" aria-controls="btnradio2">
                         3
                    </label>
                    <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
                    <label class="btn btn-outline-success" for="btnradio3" aria-label="Basic radio toggle button group" data-bs-toggle="collapse" href="#btnradio3" aria-expanded="false" aria-controls="btnradio3">
                         4
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-7 text-center">
                <div class="collapse multi-collapse" id="btnradio1">
                  <div class="card card-body bg-light mt-5">
                    <p class="text-dark h4">
                        <b>Gesamtsumme</b>
                    </p>
                    <p class="text-dark">
                        4 Gerichte/ 2 Personen pro Woche
                    </p>
                    <p class="text-dark">
                        6 Mahlzeiten für 6 € pro Portion
                    </p>
                    <hr class="border border-1 border-dark">
                    <div class="row">
                        <div class="col-6 text-start">
                            <p class="text-dark">
                                Box Preis
                            </p>
                            <p class="text-dark">
                                Versand 
                            </p>
                            <p class="text-dark">
                               <b>1. Box kost </b>
                            </p>
                        </div>
                        <div class="col-6 text-end">
                            <p class="text-dark">
                                <b>32,40 ¢</b>
                            </p>
                            <p class="text-dark">
                                <b>4.50 ¢</b>
                            </p>
                            <p class="text-dark">
                                <b>36.90 ¢</b>
                            </p>
                        </div>
                    </div>
                    <button class="btn btn-outline-success bg-success text-light">
                        Auswählen
                    </button>
                  </div>
                </div>

                <div class="collapse multi-collapse" id="btnradio2">
                  <div class="card card-body bg-light mt-5">
                    <p class="text-dark h4">
                        <b>Gesamtsumme</b>
                    </p>
                    <p class="text-dark">
                        2 Gerichte/ 3 Personen pro Woche
                    </p>
                    <p class="text-dark">
                        6 Mahlzeiten für 7 € pro Portion
                    </p>
                    <hr class="border border-1 border-dark">
                    <div class="row">
                        <div class="col-6 text-start">
                            <p class="text-dark">
                                Box Preis
                            </p>
                            <p class="text-dark">
                                Versand 
                            </p>
                            <p class="text-dark">
                               <b>1. Box kost </b>
                            </p>
                        </div>
                        <div class="col-6 text-end">
                            <p class="text-dark">
                                <b>40,40 ¢</b>
                            </p>
                            <p class="text-dark">
                                <b>4.50 ¢</b>
                            </p>
                            <p class="text-dark">
                                <b>44.90 ¢</b>
                            </p>
                        </div>
                    </div>
                    <button class="btn btn-outline-success bg-success text-light">
                        Auswählen
                    </button>
                  </div>
                </div>



                <div class="collapse multi-collapse" id="btnradio3">
                  <div class="card card-body bg-light mt-5">
                    <p class="text-dark h4">
                        <b>Gesamtsumme</b>
                    </p>
                    <p class="text-dark">
                        2 Gerichte/ 4 Personen pro Woche
                    </p>
                    <p class="text-dark">
                        8 Mahlzeiten für 7 € pro Portion
                    </p>
                    <hr class="border border-1 border-dark">
                    <div class="row">
                        <div class="col-6 text-start">
                            <p class="text-dark">
                                Box Preis
                            </p>
                            <p class="text-dark">
                                Versand 
                            </p>
                            <p class="text-dark">
                               <b>1. Box kost </b>
                            </p>
                        </div>
                        <div class="col-6 text-end">
                            <p class="text-dark">
                                <b>49,40 ¢</b>
                            </p>
                            <p class="text-dark">
                                <b>4.50 ¢</b>
                            </p>
                            <p class="text-dark">
                                <b>53.90 ¢</b>
                            </p>
                        </div>
                    </div>
                    <button class="btn btn-outline-success bg-success text-light">
                        Auswählen
                    </button>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 text-center">
            <p class="text-dark h2 mt-5">
                <b>Mehr als 31 frische Rezepte jede Woche</b>
            </p>
            <p class="text-dark h5">
                Wörterbuch aus über 200 Lateinischen Wörter
            </p>
            <div id="carouselExampleCaptions" class="carousel slide mt-5" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                </div>
                <div class="carousel-inner text-center">
                    <div class="carousel-item active text-center">
                        <div class="row ">
                            <div class="col-lg-2 col-md-4">
                                <div class="card">
                                    <img src="/images/tografie_portfolio_4.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            Card title
                                        </h5>
                                        <p class="card-text">
                                            This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <div class="card">
                                    <img src="/images/grafie_8.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            Card title
                                        </h5>
                                        <p class="card-text">
                                            This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <div class="card">
                                    <img src="/images/io_6.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            Card title
                                        </h5>
                                        <p class="card-text">
                                            This is a longer card with supporting text below as a natural lead-in to additional content.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <div class="card">
                                    <img src="/images/E_Carousel_Tip3_Rice.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            Card title
                                        </h5>
                                        <p class="card-text">
                                            This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <div class="card">
                                    <img src="/images/el_Tip1_Potatoes.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            Card title
                                        </h5>
                                        <p class="card-text">
                                            This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <div class="card">
                                    <img src="/images/ousel_Tip4_Ham.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            Card title
                                        </h5>
                                        <p class="card-text">
                                            This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-item active text-center">
                            <div class="row ">
                                <div class="col-lg-2 col-md-4">
                                    <div class="card">
                                        <img src="/images/schlimann_foodfotografie_5.jpg" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Card title
                                            </h5>
                                            <p class="card-text">
                                                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4">
                                    <div class="card">
                                        <img src="/images/grafie_8.jpg" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Card title
                                            </h5>
                                            <p class="card-text">
                                                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4">
                                    <div class="card">
                                        <img src="/images/io_6.jpg" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Card title
                                            </h5>
                                            <p class="card-text">
                                                This is a longer card with supporting text below as a natural lead-in to additional content.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4">
                                    <div class="card">
                                        <img src="/images/E_Carousel_Tip3_Rice.jpg" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Card title
                                            </h5>
                                            <p class="card-text">
                                                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4">
                                    <div class="card">
                                        <img src="/images/el_Tip1_Potatoes.jpg" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Card title
                                            </h5>
                                            <p class="card-text">
                                                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4">
                                    <div class="card">
                                        <img src="/images/ousel_Tip4_Ham.jpg" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                Card title
                                            </h5>
                                            <p class="card-text">
                                                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Previous
                    </span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Next
                    </span>
                </button>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 text-center">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Gutscheincode eingeben" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <button class="btn btn-outline-success" id="basic-addon2">
                        Anweden
                    </button>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <p class="text-dark h2">
                    <b>Wir sparen Dir Geld</b>
                </p>
                <img src="/images/percentage-icon.svg" class="img-fluid">
                <p class="text-dark h5">
                    Basierend auf Kundenbewertungen von
                    <img src="/images/trustpilot.png" class="img-fluid">
                </p>
            </div>
            <div class="col-lg-6 text-center">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active bg-light py-5">
                            <p class="text-dark h5">
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                            </p>
                            <p class="text-dark">
                                Es gibt viele Variationen der Passages des Lorem Ipsum, aber der Hauptteil erlitt Änderungen in irgendeiner Form, durch Humor oder zufällige Wörter welche nicht einmal ansatzweise glaubwürdig aussehen. Wenn du eine Passage des Lorem Ipsum nutzt, solltest du aufpassen dass in der Mitte des Textes keine ungewollten Wörter stehen. Viele der Generatoren im Internet neigen dazu, vorgefertigte Stücke zu wiederholen - was es nötig 
                            </p>
                        </div>
                        <div class="carousel-item bg-light py-5">
                            <p class="text-dark h5">
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                            </p>
                            <p class="text-dark">
                                Es gibt viele Variationen der Passages des Lorem Ipsum, aber der Hauptteil erlitt Änderungen in irgendeiner Form, durch Humor oder zufällige Wörter welche nicht einmal ansatzweise glaubwürdig aussehen. Wenn du eine Passage des Lorem Ipsum nutzt, solltest du aufpassen dass in der Mitte des Textes keine ungewollten Wörter stehen. Viele der Generatoren im Internet neigen dazu, vorgefertigte Stücke zu wiederholen - was es nötig 
                            </p>
                        </div>
                        <div class="carousel-item bg-light py-5">
                            <p class="text-dark h5">
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                                <i class="fas fa-star text-warning h3"></i>
                            </p>
                            <p class="text-dark">
                                Es gibt viele Variationen der Passages des Lorem Ipsum, aber der Hauptteil erlitt Änderungen in irgendeiner Form, durch Humor oder zufällige Wörter welche nicht einmal ansatzweise glaubwürdig aussehen. Wenn du eine Passage des Lorem Ipsum nutzt, solltest du aufpassen dass in der Mitte des Textes keine ungewollten Wörter stehen. Viele der Generatoren im Internet neigen dazu, vorgefertigte Stücke zu wiederholen - was es nötig 
                            </p>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Previous
                    </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <p class="text-dark h3 mt-5">
                    <b>3 gute Gründe für ......</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4 text-start">
                <p class="text-dark h5">
                    <b>Wo kommt es her?</b>
                </p>
                <p class="text-dark h5 text-start mt-4">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der
                </p>
            </div>
            <div class="col-lg-4 text-start">
                <p class="text-dark h5">
                    <b>Wir sorgen für Abwechslung</b>
                </p>
                <p class="text-dark h5 text-start mt-4">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der
                </p>
            </div>
            <div class="col-lg-4 text-start">
                <p class="text-dark h5">
                    <b>Wir liefern Top-Qualität</b>
                </p>
                <p class="text-dark h5 text-start mt-4">
                    Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center">
            <hr class="border border-1 border-dark">
             <p class="text-dark h3">
                <b>Bleib informiert</b>
            </p>
            <p class="text-dark h5">
                Glauben oder nicht glauben, Lorem Ipsum ist nicht
            </p>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4 text-center">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="E Mail-Adresse hier eingeben" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <button class="btn btn-outline-success" id="basic-addon2">
                    Anmelden
                </button>
            </div>
        </div>
    </div>
</div>
