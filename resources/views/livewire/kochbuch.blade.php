<div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center mt-5">
                <input type="" name="" class="input-group border border-1 border-dark">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-dark h1 text-start mb-3">
                    <b>Internationale Küchen</b>
                </p>
                <div id="carouselExampleSlidesOnly" class="carousel slide " data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <button class="btn btn-outline-success border border-2 border-success   text-dark" style="border-radius:25px;">
                                Italienische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Rezepte aus dem Nahen Osten
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Mexikanische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Afrikanische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Amerikanische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Türkische Rezepte
                          </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Marokkanische Rezepte
                          </button>
                        </div>
                        <div class="carousel-item">
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Fusionsrezepte
                          </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Vietnamesische Rezepte
                          </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-dark h1 text-start">
                    <b>Fusion Küche</b>
                </p>
                <p class="text-dark h5 mt-3 text-start">
                    Für die Schinkenfleckerl in einem großen Topf Salzwasser zum Kochen bringen, die Fleckerl zufügen und al dente (bissfest) kochen,
                </p>
                <p class="text-dark h5 text-start">
                    danach abseihen und mit kalten Wasser abschrecken.
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/h1.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/h2.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vegetarische Enchiladas
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/h3.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/h4.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/m1.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/m2.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vegetarische Enchiladas
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/m3.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/m4.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-dark h1 text-start mb-3">
                    <b>Internationale Küchen</b>
                </p>
                <div id="carouselExampleSlidesOnly" class="carousel slide " data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <button class="btn btn-outline-success border border-2 border-success   text-dark" style="border-radius:25px;">
                                Italienische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Rezepte aus dem Nahen Osten
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Mexikanische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Afrikanische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Amerikanische Rezepte
                            </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Türkische Rezepte
                          </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Marokkanische Rezepte
                          </button>
                        </div>
                        <div class="carousel-item">
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Fusionsrezepte
                          </button>
                            <button class="btn btn-outline-success border border-2 border-success  text-dark" style="border-radius:25px;">
                                Vietnamesische Rezepte
                          </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k1.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k2.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vegetarische Enchiladas
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k3.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k4.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k5.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k6.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vegetarische Enchiladas
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k7.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/Kochbuch/k1.jpg" class="img-fluid">
                <p class="text-dark h5 mt-3 text-start">
                    Vietnamesische Rezepte
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <button class="btn btn-outline-success bg-success text-light mt-3" style="width:230px;">
                    Mehr anzeigen
                </button>
                <p class="text-dark h1 text-start mt-5">
                    <b>Es ist ein lang erwiesener Fakt</b>
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-3 text-center">
                <img src="/images/el_Tip1_Potatoes.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Kartoffelpüree</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Kartoffelpüree oder Erdäpfelpüree
                </p>
                <p class="text-dark h5">
                     passt zu zahlreichen Gerichten
                </p>
                <p class="text-dark h5">
                    und kann je nach Belieben
                </p>
                <p class="text-dark h5">
                    Für ein Kartoffelpüree
                </p>
                <p class="text-dark h5">
                    sollten mehlige Kartoffeln
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/E_Carousel_Tip3_Rice.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Reis</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Zwiebel schälen,
                </p>
                <p class="text-dark h5">
                    in feine Stücke schneiden.
                </p>
                <p class="text-dark h5">
                    In einem großen Topf
                </p>
                <p class="text-dark h5">
                    die Butter zerlassen 
                </p>
                <p class="text-dark h5">
                    und erhitzen
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/sel_Tip3_Chicken.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Hähnchenfleisch</b>
                </p>
                <p class="text-dark h5 mt-3">
                   Zuerst Erbsenschoten auftauen
                </p>
                <p class="text-dark h5">
                    waschen und beiseite stellen.
                </p>
                <p class="text-dark h5">
                    Das Fleisch klein schneiden
                </p>
                <p class="text-dark h5">
                    und im Wok mit etwas 
                </p>
                <p class="text-dark h5">
                    und erhitzen Sesamöl  
                </p>
                <p class="text-dark h5">
                    einige Minuten gut 
                </p>
            </div>
            <div class="col-lg-3 text-center">
                <img src="/images/ousel_Tip4_Ham.jpg" class="img-fluid">
                <p class="text-dark h3 mt-5">
                    <b>Schinken</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Für die Schinkenfleckerl
                </p>
                <p class="text-dark h5">
                     in einem großen Topf
                </p>
                <p class="text-dark h5">
                     Salzwasser zum Kochen
                </p>
                <p class="text-dark h5">
                    bringen, die Fleckerl  
                </p>
                <p class="text-dark h5">
                    zufügen
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <hr class="border border-1 border-dark">
        </div>
    </div>
</div>
